const express = require('express');
const path = require('path');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
const keys = require('./config/keys');

/**
 * * Connect to DB
 */

mongoose.connect(keys.MONGO_URI, { useNewUrlParser: true })
  .then(() => console.log('Database connected'))
  .catch(err => console.log(err))

/**
 * * Initialize passportjs
 */

app.use(passport.initialize());
require('./middleware/passport')(passport);  

/**
 * * Connect middlewares
 */

app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(cors());

/**
 * * Connect routes
 */

const authRoute = require('./routes/auth');
const analyticsRoute = require('./routes/analytics');
const categoryRoute = require('./routes/category');
const orderRoute = require('./routes/order');
const positionRoute = require('./routes/position');

/**
 * * Add middlewares
 */

app.use('/api/auth', authRoute);
app.use('/api/analytics', analyticsRoute);
app.use('/api/category', categoryRoute);
app.use('/api/order', orderRoute);
app.use('/api/position', positionRoute);

if(process.env.NODE_ENV === 'production') {
  app.use(express.static('client/dist/client'));

  app.get('*', (req, res) => {
    res.sendFile(
      path.resolve(
        __dirname, 'client', 'dist', 'client', 'index.html'
      )
    )
  })
}

module.exports = app;