import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { AnalyticsService } from '../shared/services/analytics.service';
import { Subscription, Observable } from 'rxjs';
import { OverviewPage } from '../shared/interfaces';
import { MaterialInstance, MaterialService } from '../shared/classes/material.service';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.css']
})
export class OverviewPageComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('tapTarget') tapTargetRef: ElementRef;
  aSub: Subscription;
  data$: Observable<OverviewPage>;
  tapTarget: MaterialInstance;
  yesterday: Date = new Date();

  constructor(
    private overviewService: AnalyticsService
  ) { }

  ngOnInit() {
    this.data$ = this.overviewService.getOverview();

    this.yesterday.setDate(this.yesterday.getDate() - 1);
  }

  ngOnDestroy(){
    this.tapTarget.destroy();
  }

  ngAfterViewInit(){
    this.tapTarget = MaterialService.InitTapTarget(this.tapTargetRef);
  }

  openInfo() {
    this.tapTarget.open();
  }

}
