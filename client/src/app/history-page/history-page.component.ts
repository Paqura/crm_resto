import { Component, OnInit, ViewChild, ElementRef, OnDestroy,  AfterViewInit } from '@angular/core';
import { MaterialInstance, MaterialService } from '../shared/classes/material.service';
import { OrdersService } from '../shared/services/orders.service';
import { Subscription } from 'rxjs';
import { Order, Filter } from '../shared/interfaces';


const STEP = 2;
@Component({
  selector: 'app-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.css']
})
export class HistoryPageComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('tooltip') tooltipRef: ElementRef;
  orders: Order[] = [];
  oSub: Subscription;
  tooltip: MaterialInstance;
  isFilterVisible: boolean = false;
  offset = 0;
  limit = STEP;
  isReloading = false;
  isLoading = false;
  noMoreOrders = false;
  filter: Filter = {};

  constructor(
    private ordersService: OrdersService
  ) { }

  ngOnInit() {
    this.isReloading = true;
    this.fetch();
  }

  ngAfterViewInit(){
    this.tooltip = MaterialService.initTooltip(this.tooltipRef);
  }

  ngOnDestroy(){
    this.tooltip.destroy();
    if(this.oSub) {
      this.oSub.unsubscribe();
    }
  }

  private fetch() {
    const params = Object.assign({}, this.filter, {
      offset: this.offset,
      limit: this.limit
    })

    this.oSub = this.ordersService.fetch(params).subscribe((orders) => {
      this.orders = this.orders.concat(orders);
      this.noMoreOrders = orders.length < STEP;
      this.isLoading = false;
      this.isReloading = false;
    });
  }

  loadMore() {
    this.isLoading = true;
    this.offset += STEP;
    this.fetch();
  }

  applyFilter(filter: Filter) {
    this.orders = [];
    this.offset = 0;
    this.filter = filter;
    this.isReloading = true;
    this.fetch();
  }

  isEmptyFilter():boolean {
    return Object.keys(this.filter).length === 0;
  }

}
