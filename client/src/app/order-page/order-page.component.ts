import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MaterialInstance, MaterialService } from '../shared/classes/material.service';
import { OrderService } from './order.service';
import { OrderPosition, Order } from '../shared/interfaces';
import { OrdersService } from '../shared/services/orders.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css'],
  providers: [OrderService]
})
export class OrderPageComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('modal') modalRef: ElementRef;
  isRoot: boolean;
  modal: MaterialInstance;
  pending = false;
  oSub: Subscription;

  constructor(
    private router: Router,
    public orderService: OrderService,
    private ordersService: OrdersService
  ) { }

  ngOnInit() {
    this.isRoot = this.router.url === '/order';
    this.router.events.subscribe((event) => {
      if(event instanceof NavigationEnd) {
        this.isRoot = this.router.url === '/order';
      }
    })
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  ngOnDestroy() {
    this.modal.destroy();
    if(this.oSub) {
      this.oSub.unsubscribe();
    }
  }


  removeOrderPosition(position: OrderPosition) {
    this.orderService.remove(position);
  }

  open() {
    this.modal.open();
  }

  cansel() {
    this.modal.close();
  }

  submit() {
    this.pending = true;
    this.modal.close();

    /**
     * Так как на бекенде id не ожидается
     * он нужен только для UI,
     * удаляем его из массива
     */

    const newOrder: Order = {
      list: this.orderService.list.map(item => {
        delete item._id;
        return item;
      })
    };

    this.oSub = this.ordersService.create(newOrder).subscribe(
      (order) => {
        MaterialService.toast(`Заказ №${order.order} был добавлен`);
        this.orderService.clear();
      },
      error => MaterialService.toast(error.error.message),
      () => {
        this.modal.close();
        this.pending = false;
      }
    )
  }

}
