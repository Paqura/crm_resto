import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from '../../shared/services/categories.service';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { MaterialService } from '../../shared/classes/material.service';
import { Category } from '../../shared/interfaces';
import { ImagePreview } from '../../shared/types';

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit {
  @ViewChild('inputFile') inputFileRef;
  category: Category;
  image: File;
  imagePreview: ImagePreview;
  form: FormGroup;
  isNew = true;

  constructor(
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required])
    })

    this.form.disable();

    this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
            if(params['id']) {
              this.isNew = false;
              return this.categoriesService.getById(params['id'])
            }
            return of(null);
          }
        )
      ).subscribe(
        (category: Category) => {
          if(category) {
            /**
             * Метод изменяет состояние инпута,
             * в данном случае value
             */
            this.category = category;
            this.form.patchValue({
              name: category.name
            })
            this.imagePreview = category.imageSrc;
            MaterialService.updateTextInputs();
          }
          this.form.enable();
        },
        error => MaterialService.toast(error.error.message)
      )
  }

  deleteCategory() {
    const decision = window.confirm(`Вы уверены, что хотите удалить категорию ${this.category.name}`);

    if(decision) {
      this.categoriesService.delete(this.category._id)
        .subscribe(
          response => MaterialService.toast(response.message),
          error => MaterialService.toast(error.message),
          () => this.router.navigate(['/categories'])
        )
    }
  }

  triggerInputFile() {
    this.inputFileRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.image = file;

    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result;
    };
    reader.readAsDataURL(file);
  }

  onSubmit() {
    let observer$;
    this.form.disable();

    if(this.isNew) {
      observer$ = this.categoriesService.create(this.form.value.name, this.image);
    } else {
      observer$ = this.categoriesService.update(this.category._id, this.form.value.name, this.image);
    }

    observer$.subscribe(
      (category: Category) => {
        this.category = category;
        MaterialService.toast('Изменения сохранены');
        this.form.enable();
      },
      error => {
        this.form.enable();
        MaterialService.toast(error.error.message);
      }
    )
  }

}
