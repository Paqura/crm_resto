const Category = require('../models/Category');
const Position = require('../models/Position');
const errorHandler = require('../utils/errorHandler');
const STATUS  = require('../config/httpStatus');
const MESSAGES  = require('../config/messages');

module.exports.getAll = async function(req, res) {
  try {
    const categories = await Category.find({ user: req.user.id });
    res.status(STATUS.OK).json(categories);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.getById = async function(req, res) {
  try {
    const category = await Category.findById(req.params.id);
    res.status(STATUS.OK).json(category);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    await Category.remove({ _id: req.params.id });
    await Position.remove({ category: req.params.id });

    res.status(STATUS.OK).json({
      message: MESSAGES.CATEGORY.IS_DELETE
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {

  const category = new Category({
    name: req.body.name,
    imageSrc: req.file ? req.file.path : '',
    user: req.user.id
  });

  try {
    await category.save();
    res.status(STATUS.CREATED).json(category);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  let updatedFields = {
    name: req.body.name
  };

  if(req.file) {
    updatedFields.imageSrc = req.file.path;
  }
  
  try {
    const category = await Category
      .findOneAndUpdate(
        { _id: req.params.id },
        { $set: updatedFields },
        { new: true }
      );
    
    res.status(STATUS.OK).json(category);
  } catch (error) {
    errorHandler(res, error);
  }
};