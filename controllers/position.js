const Position = require('../models/Position');
const errorHandler = require('../utils/errorHandler');
const STATUS = require('../config/httpStatus');
const MESSAGES = require('../config/messages');

module.exports.getByCategoryId = async function(req, res) {
  try {
    const positions = await Position.find({
      category: req.params.categoryId,
      user: req.user.id
    });

    res.status(STATUS.OK).json(positions);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {
  try {
    const position = await new Position({
      name: req.body.name,
      cost: req.body.cost,
      category: req.body.category,
      user: req.user.id
    }).save();

    res.status(STATUS.CREATED).json(position);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    await Position.remove({_id: req.params.id});
    res.status(STATUS.OK).json({
      message: MESSAGES.POSITION.IS_DELETE
    })
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  try {
    const updatedPosition = await Position.findOneAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true }
    );  

    res.status(STATUS.OK).json(updatedPosition);
  } catch (error) {
    errorHandler(res, error);
  }
};
