const Order = require('../models/Order');
const errorHandler = require('../utils/errorHandler');
const STATUS  = require('../config/httpStatus');

module.exports.getAll = async function(req, res) {
  const query = {
    user: req.user.id    
  };

  if(req.query.start) {
    query.date = {
      // Больше или равно
      $gte: req.query.start
    };
  }

  if(req.query.end) {
    if(!query.date) {
      query.date = {};
    }

    query.date['$lte'] = req.query.end;
  }

  if(req.query.order) {
    query.order = Number(req.query.order);
  }

  try {
    const orders = await Order
      .find(query)
      .sort({ date: -1 })
      .skip(Number(req.query.offset))
      .limit(Number(req.query.limit));

    res.status(STATUS.OK).json(orders);   
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {
  try {
    /**
     * * Сортировка по дате
     */
    const lastOrder = await Order
      .findOne({ user: req.user.id })
      .sort({ date: -1 });

    const maxOrder = lastOrder ? lastOrder.order : 0;  

    const order = await new Order({
      user: req.user.id,
      list: req.body.list,
      order: maxOrder + 1
    }).save();

    res.status(STATUS.CREATED).json(order);
  } catch (error) {
    errorHandler(res, error);
  }
};
